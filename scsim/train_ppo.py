from pathlib import Path

import ray
from loguru import logger
from ray import tune

from scsim.experiment import load_configs
from scsim.supply_chain_env import OWMREnvironment

from yaml import load
import yaml

def train(tune_config, env_train_config, env_eval_config, output_dir):
    ray.init(object_store_memory=8e9, num_cpus=64)
    tune.run(
        tune_config['run'],
        num_samples=1,
        restore=tune_config.get("restore", None),
        resume=tune_config.get("resume", False),
        stop=tune_config.get('stop', None),
        checkpoint_freq=tune_config.get('checkpoint_freq', 20),
        checkpoint_at_end=tune_config.get('checkpoint_at_end', True),
        local_dir=output_dir,
        config={
            **tune_config["config"],
            "env": OWMREnvironment,
            "env_config": env_train_config,
            "evaluation_config": {"env_config": env_eval_config, "explore": False},
        },
        # restore=join(path_to_root,
        #              'ray_results/PPO/PPO_GymWrapper_0_2020-07-28_09-43-20zkhpvap3/checkpoint_1875/checkpoint-1875')
    )



import argparse

parser = argparse.ArgumentParser(description="Process some integers.")
parser.add_argument(
    "-s", "--scenario", type=str, help="root experiment folder",
)


if __name__ == "__main__":
    parser.add_argument(
        '--resume', type=str,
    )
    parser.add_argument(
        "--restore", type=str
    )
    args = parser.parse_args()

    scenario = args.scenario
    resume = args.resume
    restore = args.restore
    logger.info('Running Training for {}'.format(scenario))
    import scsim
    PROJECT_ROOT = Path(scsim.__file__).parent.parent
    CONFIGS_DIR = PROJECT_ROOT / 'configs'
    scenario_folder = CONFIGS_DIR / scenario
    scenario_id = scenario_folder.name.split('_')[-1]
    experiment_folder = PROJECT_ROOT / 'experiments'
    tune_config_path = CONFIGS_DIR / 'rllib_baselines' / 'ppo_config.yaml'

    tune_config = load(
        open(tune_config_path), Loader=yaml.FullLoader
    )

    default_env_config, env_eval_config, env_train_config = load_configs(scenario_folder)

    output_dir = experiment_folder / f'scenario_{scenario_id}'
    output_dir.mkdir(exist_ok=True, parents=True)
    with open(output_dir / 'default_config.yaml', 'w') as f:
        yaml.dump(default_env_config, f)
    with open(output_dir / 'evaluation_config.yaml', 'w') as f:
        yaml.dump(env_eval_config, f)
    with open(output_dir / 'training_config.yaml', 'w') as f:
        yaml.dump(env_train_config, f)
    tune_config['resume'] = args.resume
    tune_config['restore'] = args.restore
    train(tune_config, env_train_config, env_eval_config, output_dir=output_dir / 'ray_results')

