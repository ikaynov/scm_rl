import ray
from ray import tune

from scsim.supply_chain_env import OWMREnvironment

from yaml import load
import yaml

from os.path import join

import argparse

parser = argparse.ArgumentParser(description="Process some integers.")
parser.add_argument(
    "-r", "--root_dir", type=str, help="root experiment folder",
)

parser.add_argument(
    "--tune_config", type=str, help="name of the tune config", default="ppo_config.yaml"
)


if __name__ == "__main__":
    # Define folder and file names
    args = parser.parse_args()
    ROOT_FOLDER = args.root_dir
    print(ROOT_FOLDER)
    DEFAULT_CONFIG = "default_config.yaml"
    EVALUATION_CONFIG = "evaluation_config.yaml"
    TRAINING_CONFIG = "training_config.yaml"

    TUNE_CONFIG = args.tune_config

    path_to_root = join("experiments", ROOT_FOLDER)

    # The configurations will be recursively overwritten
    # default_config <- eval_config <- train_config
    # so that evaluation and training configs can be missing
    # or have only those values that should be overwritten
    from scsim.experiment import load_configs

    _, env_eval_config, env_train_config = load_configs(path_to_root)

    tune_config = load(
        open(join(path_to_root, "rllib_baselines", TUNE_CONFIG)), Loader=yaml.FullLoader
    )

    ray.init(object_store_memory=8e9)
    tune.run(
        tune_config['run'],
        restore=tune_config.get("restore", None),
        resume=tune_config.get("resume", False),
        stop=tune_config.get('stop', None),
        checkpoint_freq=tune_config.get('checkpoint_freq', 20),
        checkpoint_at_end=tune_config.get('checkpoint_at_end', True),
        local_dir=join(path_to_root, "ray_results"),
        config={
            **tune_config["config"],
            "env": OWMREnvironment,
            "env_config": env_train_config,
            "evaluation_config": {"env_config": env_eval_config, "explore": False},
        },
        # restore=join(path_to_root,
        #              'ray_results/PPO/PPO_GymWrapper_0_2020-07-28_09-43-20zkhpvap3/checkpoint_1875/checkpoint-1875')
    )
